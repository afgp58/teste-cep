<h1>Busca por CEP</h1>

### Descrição

<p>Esse projeto tem o objetivo de realizar buscas de endereço preenchendo o campo do CEP, utilizando a API do viacep.</p>

### Pré-Requisitos

Para executar o projeto é necessario ter as seguintes ferramentas instaladas. [GitLab](https://about.gitlab.com/) o [node.js](https://nodejs.org/en/) e alguma IDE de sua preferencia como o [VSCode](https://code.visualstudio.com/) .

Rodando o Front-End do projeto:

```bash
#Clone este repositorio
git clone <https://gitlab.com/afgp58/teste-cep.git>

#Acesse a pasta do projeto no terminal/cmd
cd teste-cep

#Instale as dependencias
npm install

# Execute a aplicação em modo de desenvolvimento
npm start

# O servidor iniciará na porta : 3000 - acesse <http://localhost:3000>
```
### Tecnologias

As seguintes ferramentas foram utilizadas na elaboração deste projeto.

- React

- Javascript

- Node.js

---
Project made by André Felipe. Aug, 2022
