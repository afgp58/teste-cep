import React from 'react';
import { Formik, Field, Form } from 'formik';
import './App.css';
// Funçoes //
function App() {
  function onSubmit(values, actions) {
    console.log('SUBMIT', values);
  }

  function onBlurCep(ev, setFieldValue) {
    const { value } = ev.target;

    const cep = value?.replace(/[^0-9]/g, '');

    if (cep?.length !== 8) {
      return;
    }
// Fetch de acesso a API //
    fetch(`https://viacep.com.br/ws/${cep}/json/`)
      .then((res) => res.json())
      .then((data) => {
        setFieldValue('logradouro', data.logradouro);
        setFieldValue('bairro', data.bairro);
        setFieldValue('cidade', data.localidade);
        setFieldValue('uf', data.uf);
      });
  }
//Formik e Configuraçao dos elementos//
  return (
    <div className="App">
      <h1>CrossWork Busca por CEP</h1>
      <Formik
        onSubmit={onSubmit}
        validateOnMount
        initialValues={{
          cep: '',
          logradouro: '',
          numero: '',
          complemento: '',
          bairro: '',
          cidade: '',
          uf: '',
        }}
        render={({ isValid, setFieldValue }) => (
          <Form>
            <div className="form-control-group">
              <label>CEP</label>
              <Field name="cep" type="text" onBlur={(ev) => onBlurCep(ev, setFieldValue)}/>
            </div>
            <button id="button" type="submit" disabled={!isValid}>Pesquisar</button>
            <p class="result">
              Resultado da busca
            </p>
            <div className="form-control-group">
              <label>UF</label>
              <Field name="uf" type="text" />
            </div>
            <div className="form-control-group">
              <label>Cidade</label>
              <Field name="cidade" type="text" />
            </div>
            <div className="form-control-group">
              <label>Logradouro</label>
              <Field name="logradouro" type="text" />
            </div>
            <div className="form-control-group">
              <label>Bairro</label>
              <Field name="bairro" type="text" />
            </div>
            <div className="form-control-group">
              <label>Número</label>
              <Field name="numero" type="text" />
            </div>
            <div className="form-control-group">
              <label>Complemento</label>
              <Field name="complemento" type="text" />
            </div>
          </Form>
        )}
            
      />
    </div>
  );
}
              

export default App;
              
            
            
            
            
            
